## 更新日志

### 2013-07-16

新增：

- 百度统计 http://tongji.baidu.com
- cnzz http://www.cnzz.com
- bShare http://www.bshare.cn
- 百度分享 http://share.baidu.com
- 多说 http://www.duoshuo.com
- 友荐 http://www.ujian.cc
- 友言 http://www.uyan.cc
- JiaThis http://www.jiathis.com
- 无觅相关文章插件 http://www.wumii.com/widget/getWidget

### 2013-11-11

V 0.3.2

新增：

- Angular http://angularjs.org
- HumansTxt http://humanstxt.org
- Ning http://www.ning.com
- ektron http://www.ektron.com
- Mura CMS http://www.getmura.com
- Tiki Wiki CMS Groupware http://info.tiki.org
- etracker http://etracker.com
- OpenTag http://opentag.qubitproducts.com
- SPDY http://www.chromium.org/spdy
- KISSmetrics http://kissmetrics.com
- LiveStreet http://livestreetcms.com
- PHP http://php.net
- Apache http://httpd.apache.org
- nginx http://nginx.org
- Varnish https://www.varnish-cache.org
- IIS http://www.iis.net
- ASP.NET http://www.asp.net
- Nette http://nette.org

V 0.3.3

- CodeIgniter http://codeigniter.com

V 0.3.4

- 新增 `popup.html` 页面